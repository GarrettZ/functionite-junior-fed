window.onload = function(){

    var modal = {
            root: document.getElementById('modal-box'),
            close: document.getElementsByClassName('modal-box__close')[0],
            revealBtns: document.getElementsByClassName('show-modal')[0]
    };

    modal.revealBtns.addEventListener('click', function(){
        modal.root.classList.add('visible');
    });

    modal.close.addEventListener('click', function(){
       modal.root.classList.remove('visible');
    });
};